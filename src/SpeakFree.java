package com.example.midterm;



import com.sun.speech.freetts.Voice;
import com.sun.speech.freetts.VoiceManager;

import javax.speech.Central;
import javax.speech.EngineException;


public class SpeakFree {
    public static void speakText(String string) throws EngineException {
        System.setProperty("freetts.voices", "com.sun.speech.freetts.en.us" + ".cmu_us_kal.KevinVoiceDirectory");
        Central.registerEngineCentral("com.sun.speech.freetts" + ".jsapi.FreeTTSEngineCentral");
        Voice voice;
        voice = VoiceManager.getInstance().getVoice("kevin");
        if (voice != null) {
            voice.allocate();//Allocating Voice
        }
        try {
            assert voice != null;
            voice.setRate(200);
            voice.setPitch(150);
            voice.setVolume(10);
            voice.speak(string);

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
