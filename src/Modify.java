package com.example.midterm;

import com.jfoenix.controls.JFXButton;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.textfield.TextFields;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

import static com.example.midterm.HandleFile.translate;
import static com.example.midterm.HandleFile.words;

public class Modify implements Initializable {

    @FXML
    private JFXButton btnDelete;

    @FXML
    private JFXButton btnDeleteWord;

    @FXML
    private JFXButton btnInsert;

    @FXML
    private JFXButton btnModify;

    @FXML
    private JFXButton btnOffline;

    @FXML
    private JFXButton btnOnline;

    @FXML
    private JFXButton btnPara;

    @FXML
    private JFXButton btnSpeak1;

    @FXML
    private Label hiddenMenu;
    @FXML
    private ImageView imageDic;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;

    @FXML
    private ImageView imageDelete;

    @FXML
    private ImageView imageFav;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageOffline;

    @FXML
    private ImageView imageOnline;
    @FXML
    private ImageView imageSearch;

    @FXML
    private Label showMenu;

    @FXML
    private AnchorPane sld;

    @FXML
    private AnchorPane slider;

    @FXML
    private TextField textModifyMean;

    @FXML
    private TextField textModifyWord;

    @FXML
    private TextField textSearch;

    @FXML
    private TextArea modifyMeaning;

    @FXML
    void modifyWord(ActionEvent event) throws IOException {
        String target = textSearch.getText(); //
        String d1 = textModifyWord.getText(); //key ban dau
        String d2 = modifyMeaning.getText(); // mean
        String d3 = textModifyMean.getText();// spell

        String valueWord1 = translate.get(target);
        String[] p1 = valueWord1.split("\n", 2);
        String s1 = target; // new word ban dau
        String s2 = p1[1]; // nghia ban dau
        String s3 = p1[0]; // spell ban dau






        if (d1.equals(s1) && d2.equals(s2) && d3.equals(s3)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo!");
            alert.setHeaderText("Không có thay đổi!");
            alert.setContentText("Bạn chưa thay đổi gì!");
            alert.showAndWait();
        } else if (d1.isEmpty() && d2.isEmpty() && d3.isEmpty()){
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo!");
            alert.setHeaderText("Không có thay đổi!");
            alert.setContentText("Bạn chưa thay đổi gì!");
            alert.showAndWait();
        } else{
            String d1l = textModifyWord.getText(); //key
            String d2l = modifyMeaning.getText(); // mean
            String d3l = textModifyMean.getText();// spell
            String value;
            value = d3l + "\n" + d2l;

            HandleFile.translate.remove(target);
            HandleFile.words.remove(target);
            HandleFile.translate.put(d1l, value);
            HandleFile.words.add(d1l);
            TextFields.bindAutoCompletion(textSearch, words);

            FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/big.txt"));
            for (Map.Entry mapElement : HandleFile.translate.entrySet()) {
                String updateWord = (String) mapElement.getKey();
                String updateMean = (String) mapElement.getValue();
                insetFileWriter.append("\n@").append(updateWord);
                insetFileWriter.append(" ").append(updateMean).append("\n");
            }
            insetFileWriter.close();


            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Thành Công!");
            alert.setContentText("Cập nhật thành công!");
            alert.showAndWait();
            textSearch.clear();
            textModifyWord.clear();
            textModifyMean.clear();
            modifyMeaning.clear();


        }

    }



    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());
    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());
    Image search = new Image(new File("src/main/java/image/search.png").toURI().toString());




    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageFav.setImage(favourite);
        imageIcon.setImage(menuIcon);
        imageDic.setImage(dic);
        imageIconBack.setImage(menuIcon);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);
        imageSearch.setImage(search);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }

        TextFields.bindAutoCompletion(textSearch, words);
        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });

    }

    @FXML
    void switchOffline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchOnline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOnline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchDelete(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("delete.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchInsert(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("insert.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchFavourite(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("favourite.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void searchWord(ActionEvent event) throws IOException {
        String target = textSearch.getText();
        if (translate.containsKey(target)) {
            //textShow.setText(translate.get(target));
            String valueWord1 = translate.get(target);
            String[] p1 = valueWord1.split("\n", 2);
            String s1 = target; // new word
            String s2 = p1[1]; // nghia
            String s3 = p1[0]; // spell
            textModifyWord.setText(target);
            textModifyMean.setText(s3);
            modifyMeaning.setText(s2);


//            textSearch.clear();


        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo");
            alert.setHeaderText("Chưa có từ");
            alert.setContentText("Từ bạn cần chỉnh sửa chưa có trong từ điển!");
            alert.showAndWait();
        }

    }

}
