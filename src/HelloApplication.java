package com.example.midterm;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * Nhóm 15: Trần Huỳnh Đức.
 * Nông Đức Việt Anh.
 * Phạm Hoàng Anh.
 * Phạm Tiến Dũng.
 */

public class HelloApplication extends Application implements IExitable {
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);

        String css = this.getClass().getResource("textField.css").toExternalForm();
        scene.getStylesheets().add(css);
//        scene.setFill(Color.TRANSPARENT);


        Image search = new Image(new File("src/main/java/image/smiles.png").toURI().toString());
        stage.getIcons().add(search);
        stage.setTitle("Anh Anh Dũng Đức");

//        stage.initStyle(StageStyle.TRANSPARENT);

        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(event -> { this.exitClick();
            event.consume();
        } );
    }

    public static void main(String[] args) throws IOException {
        Language.init();
        HandleFile.handleFile();
        System.out.println(HandleFile.words.size());
        launch();
    }


}