package com.example.midterm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Language {

    public static HashMap<String,String> hashLanguage = new HashMap<String,String>();
    public static List<String> listLanguage = new ArrayList<String>();
    public static void init() {
        hashLanguage.put("VIETNAMESE", "vi");
        hashLanguage.put("ENGLISH", "en");
        hashLanguage.put("AFRIKAANS", "af");
        hashLanguage.put("ALBANIAN", "sq");
        hashLanguage.put("INDONESIAN", "id");

        hashLanguage.put("CROATIAN", "hr");
        hashLanguage.put("FRENCH", "fr");
        hashLanguage.put("JAPANESE", "ja");
        hashLanguage.put("KOREAN", "ko");
        hashLanguage.put("MALAY", "ms");
        hashLanguage.put("PORTUGUESE", "pt");
        hashLanguage.put("THAI", "th");
        hashLanguage.put("RUSSIAN", "ru");


        listLanguage.add("VIETNAMESE");
        listLanguage.add("ENGLISH");
        listLanguage.add("AFRIKAANS");
        listLanguage.add("ALBANIAN");
        listLanguage.add("INDONESIAN");
        listLanguage.add("CROATIAN");
        listLanguage.add("FRENCH");
        listLanguage.add("JAPANESE");
        listLanguage.add("KOREAN");
        listLanguage.add("MALAY");
        listLanguage.add("PORTUGUESE");
        listLanguage.add("THAI");
        listLanguage.add("RUSSIAN");
        Collections.sort(listLanguage);


    }

}
