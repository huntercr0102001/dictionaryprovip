package com.example.midterm;

import com.jfoenix.controls.JFXButton;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import javax.speech.EngineException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

public class Favourite implements Initializable {

    @FXML
    private JFXButton btnDelete;

    @FXML
    private JFXButton btnInsert;

    @FXML
    private JFXButton btnModify;

    @FXML
    private JFXButton btnOffline;

    @FXML
    private JFXButton btnOnline;

    @FXML
    private JFXButton btnPara;
    @FXML
    private JFXButton btnSpeak;

    @FXML
    private Label hiddenMenu;

    @FXML
    private ImageView imageDelete;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;

    @FXML
    private ImageView imageDic;

    @FXML
    private ImageView imageFav;
    @FXML
    private ImageView imageSpeak;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageOffline;

    @FXML
    private ImageView imageOnline;

    @FXML
    private Label showMenu;

    @FXML
    private AnchorPane sld;

    @FXML
    private AnchorPane slider;
    @FXML
    private TextArea textShowMean;
    @FXML
    private TextField spell;

    @FXML
    private ListView<String> listFavourite;

    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());
    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image speak = new Image(new File("src/main/java/image/s1.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());

    public static String current = "";
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageFav.setImage(favourite);
        imageDic.setImage(dic);
        imageIcon.setImage(menuIcon);
        imageIconBack.setImage(menuIcon);
        imageSpeak.setImage(speak);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }

        textShowMean.setEditable(false);
        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });



        listFavourite.getItems().clear();
        listFavourite.getItems().addAll(HandleFile.fWord);
        listFavourite.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                current = listFavourite.getSelectionModel().getSelectedItem();
//                textShowMean.setText(HandleFile.favourite.get(current));
                String valueWord = HandleFile.favourite.get(current);
                String[] p = valueWord.split("\n", 2);
                spell.setText(p[0]);
                textShowMean.setText(p[1]);
            }
        });

    }

    @FXML
    void switchOffline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchOnline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOnline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchInsert(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("insert.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchDelete(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("delete.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchModify(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("modify.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void deleteFavourite(ActionEvent event) throws IOException {

        if (current.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Lỗi cú pháp!");
            alert.setContentText("Chưa có từ nào được chọn!");
            alert.showAndWait();
        } else {
            Alert comfirmDelete = new Alert(Alert.AlertType.CONFIRMATION);
            comfirmDelete.setTitle("Cảnh báo");
            comfirmDelete.setHeaderText("Cảnh báo!");
            comfirmDelete.setContentText("Bạn chắc chắn muỗn xóa không?");
            ButtonType buttonTypeYes = new ButtonType("Chắc Chắn", ButtonBar.ButtonData.YES);
            ButtonType buttonTypeNo = new ButtonType("Thoát", ButtonBar.ButtonData.NO);

            comfirmDelete.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
            Optional<ButtonType> result = comfirmDelete.showAndWait();
            if (result.get() == buttonTypeYes) {
                HandleFile.favourite.remove(current);
                HandleFile.fWord.remove(current);
                FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/favourite.txt"));
                for (Map.Entry mapElement : HandleFile.favourite.entrySet()) {
                    String updateWord = (String) mapElement.getKey();
                    String updateMean = (String) mapElement.getValue();
                    insetFileWriter.append("@").append(updateWord);
                    insetFileWriter.append(" ").append(updateMean).append("\n");

                }
                insetFileWriter.close();

                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText("Xóa từ thành công!");
                a1.show();

                listFavourite.getItems().remove(current);

            } else {
                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText(message);

                a1.show();
            }
        }


    }


    @FXML
    void speakWord1(ActionEvent event) throws EngineException {
        SpeakFree.speakText(current);
    }




}
