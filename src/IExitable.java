package com.example.midterm;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;


public interface IExitable
{
    @FXML
    default void exitClick()
    {
        Alert alert = new Alert (Alert.AlertType.CONFIRMATION,
                "Bạn muốn thoát phải không?");

        alert.showAndWait().ifPresent(response ->
        {
            if (response == ButtonType.OK)
            {
                Platform.exit();
            }
        });
    }
}