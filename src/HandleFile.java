package com.example.midterm;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class HandleFile {
    // list words store vocabulary
    public static List<String> words = new ArrayList<String>();

    // create tree map to store translate
    public static TreeMap<String, String> translate = new TreeMap<String, String>();
    public static TreeMap<String, String> favourite = new TreeMap<String, String>();

    public static ArrayList<String> fWord = new ArrayList<String>();
    public static ArrayList<String> hisWord = new ArrayList<String>();

    // handle file
    public static void handleFile() throws IOException {

        try {
            Path path = Paths.get("src/main/java/dictxt/big.txt");
            List<String> dataList = Files.readAllLines(path);
            ListIterator<String> itr = dataList.listIterator();


            while (itr.hasNext()) {
                String check = itr.next();
                String key = "";
                String pa = "";
                String value = "";
                if (check.startsWith("@")) {

                    String[] part = check.split("/", 2);

                    String temp = part[0].substring(1).trim();
                    if (temp.startsWith("'") || temp.startsWith("-") || temp.startsWith("(")) {
                        temp = temp.substring(1, temp.length());
                    }
                    key += temp;

                    if (part.length < 2) {
                        pa = "";
                    } else {
                        pa += "/" + part[1];
                    }
                    while (itr.hasNext()) {
                        String p1 = itr.next();
                        if (!p1.startsWith("@")) {
                            value += p1 + "\n";
                        } else {
                            translate.put(temp, value);
                            itr.previous();
                            break;
                        }
                    }
                }
                translate.put(key, pa + "\n" + value);
                words.add(key);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            Path path = Paths.get("src/main/java/dictxt/favourite.txt");
            List<String> dataList = Files.readAllLines(path);
            ListIterator<String> itr = dataList.listIterator();


            while (itr.hasNext()) {
                String check = itr.next();
                String key = "";
                String pa = "";
                String value = "";
                if (check.startsWith("@")) {

                    String[] part = check.split("/", 2);

                    String temp = part[0].substring(1).trim();
                    if (temp.startsWith("'") || temp.startsWith("-") || temp.startsWith("(")) {
                        temp = temp.substring(1, temp.length());
                    }
                    key += temp;

                    if (part.length < 2) {
                        pa = "";
                    } else {
                        pa += "/" + part[1];
                    }
                    while (itr.hasNext()) {
                        String p1 = itr.next();
                        if (!p1.startsWith("@")) {
                            value += p1 + "\n";
                        } else {
                            favourite.put(temp, value);
                            itr.previous();
                            break;
                        }
                    }
                }
                favourite.put(key, pa + "\n" + value);
                fWord.add(key);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        File hisFile = new File("src/main/java/dictxt/history.txt");


        Scanner sch = new Scanner(hisFile);


        while (sch.hasNextLine()) {
            String s = sch.nextLine();
            hisWord.add(s);
        }
        sch.close();

    }
}
