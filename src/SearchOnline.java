package com.example.midterm;


import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javazoom.jl.decoder.JavaLayerException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Objects;
import java.util.ResourceBundle;

public class SearchOnline implements Initializable {

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnInsert;

    @FXML
    private Button btnModify;

    @FXML
    private Button btnOffline;

    @FXML
    private Button btnOnline;

    @FXML
    private Button btnPara;

    @FXML
    private Button btnSearchOnline;

    @FXML
    private Button btnSpeak1;

    @FXML
    private ChoiceBox<String> choiceBoxExplain;

    @FXML
    private ChoiceBox<String> choiceBoxSource;

    @FXML
    private Label hiddenMenu;

    @FXML
    private ImageView imageDelete;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageOffline;

    @FXML
    private ImageView imageDic;

    @FXML
    private ImageView imageOnline;
    @FXML
    private ImageView imageSearchOnline;
    @FXML
    private ImageView imageSearchOnline1;

    @FXML
    private ImageView imageFav;
    @FXML
    private ImageView imageLoading;

    @FXML
    private ImageView imageSearch;

    @FXML
    private Label showMenu;

    @FXML
    private AnchorPane sld;

    @FXML
    private AnchorPane slider;

    @FXML
    private TextField textSearch;

    @FXML
    private TextArea textShow;


    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());
    Image search = new Image(new File("src/main/java/image/search.png").toURI().toString());
    Image speak = new Image(new File("src/main/java/image/s1.png").toURI().toString());
    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());



    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageFav.setImage(favourite);
        imageIcon.setImage(menuIcon);
        imageIconBack.setImage(menuIcon);
        imageSearch.setImage(search);
        imageSearchOnline.setImage(speak);
        imageSearchOnline1.setImage(speak);
        imageDic.setImage(dic);
        textShow.setEditable(false);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }



        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });

        choiceBoxSource.getItems().addAll(Language.listLanguage);
        choiceBoxExplain.getItems().addAll(Language.listLanguage);
        choiceBoxSource.setValue("ENGLISH");
        choiceBoxExplain.setValue("VIETNAMESE");


    }


    @FXML
    void switchOffline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchInsert(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("insert.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchDelete(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("delete.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void searchWord(ActionEvent event) throws IOException {

        if (choiceBoxSource.getSelectionModel().isEmpty() || choiceBoxExplain.getSelectionModel().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Chưa chọn ngôn ngữ!");
            alert.setContentText("Bạn hãy chọn ngôn ngữ cần dịch!");
            alert.showAndWait();
        } else {



            String target = textSearch.getText();
            String langFrom = Language.hashLanguage.get(choiceBoxSource.getValue());
            String langTo = Language.hashLanguage.get(choiceBoxExplain.getValue());
//            textShow.setText(GoogleAPI.translateAPI(langFrom, langTo, target));

            if (!target.isEmpty()) {
                String res = (GoogleAPI.translateAPI(langFrom, langTo, target));

                textShow.setText(res);
            }

        }
    }

    @FXML
    void speakOnlineWord(ActionEvent event) throws IOException, JavaLayerException {
        String target = textSearch.getText();
        // SpeakFree.speakText(target);
        if (!target.isEmpty()) {
            Audio u = new Audio();
            InputStream sound = u.getAudio(target, Language.hashLanguage.get(choiceBoxSource.getValue()));
            u.play(sound);
        }
    }

    @FXML
    void speakOnlineMean(ActionEvent event) throws IOException, JavaLayerException {
        String target = textShow.getText();
        // SpeakFree.speakText(target);
        if (!target.isEmpty()) {
            Audio u = new Audio();
            InputStream sound = u.getAudio(target, Language.hashLanguage.get(choiceBoxExplain.getValue()));
            u.play(sound);
        }
    }

    @FXML
    void switchModify(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("modify.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchFavourite(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("favourite.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }


}
