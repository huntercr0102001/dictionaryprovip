package com.example.midterm;

import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import javazoom.jl.decoder.JavaLayerException;
import org.controlsfx.control.textfield.TextFields;

import javax.speech.EngineException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;

import static com.example.midterm.HandleFile.*;

public class SearchOffline implements Initializable {
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnInsert;

    @FXML
    private Button btnModify;

    @FXML
    private Button btnFavourite;

    @FXML
    private Button btnOffline;

    @FXML
    private Button btnOnline;

    @FXML
    private Button btnPara;
    @FXML
    private Button btnClear;

    @FXML
    private Label hiddenMenu;

    @FXML
    private ImageView imageDelete;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageOffline;

    @FXML
    private ImageView imageOnline;

    @FXML
    private ImageView imageFav;

    @FXML
    private ImageView imageFavWord;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageDic;


    @FXML
    private ImageView imageSearch;

    @FXML
    private Label showMenu;
    @FXML
    private Label historyHidden;
    @FXML
    private Label historyShow;

    @FXML
    private AnchorPane slider;
    @FXML
    private AnchorPane anchorHistory;

    @FXML
    private TextField textSearch;
    @FXML
    private TextField spell;
    @FXML
    private Button btnSearch;
    @FXML
    private ImageView imageSpeak;
    @FXML
    private ImageView imageHistoryShow;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;
    @FXML
    private ImageView imageHistoryHidden;
    @FXML
    private TextArea textShow;
    @FXML
    private ListView<String> historyWord;
    @FXML
    private TextField wordBig;


    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());
    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());
    Image search = new Image(new File("src/main/java/image/search.png").toURI().toString());
    Image speak = new Image(new File("src/main/java/image/s1.png").toURI().toString());
    Image favouriteImage = new Image(new File("src/main/java/image/add to love.png").toURI().toString());

    Image history = new Image(new File("src/main/java/image/history.png").toURI().toString());

    public static String his = "";

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageFav.setImage(favourite);
        imageIcon.setImage(menuIcon);
        imageIconBack.setImage(menuIcon);
        imageSearch.setImage(search);
        imageSpeak.setImage(speak);
        imageDic.setImage(dic);
        imageFavWord.setImage(favouriteImage);
        imageHistoryShow.setImage(history);
        imageHistoryHidden.setImage(history);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);
        TextFields.bindAutoCompletion(textSearch, words);
        textShow.setEditable(false);
        wordBig.setEditable(false);
        spell.setEditable(false);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }

        String[] food = {"tran", "huynh", "duc", "1", "tran", "huynh", "duc", "1", "tran", "huynh"};
        historyWord.getItems().addAll(HandleFile.hisWord);


        historyWord.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                his = historyWord.getSelectionModel().getSelectedItem();

                textSearch.setText(his);

            }
        });


        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });
//#FFCC00
        anchorHistory.setTranslateX(+195);
        btnClear.setVisible(false);
        historyHidden.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(anchorHistory);

            slide.setToX(195);
            slide.play();
            btnClear.setVisible(false);
            anchorHistory.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                historyHidden.setVisible(false);
                historyShow.setVisible(true);

            });
        });

        historyShow.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(anchorHistory);

            slide.setToX(0);
            slide.play();
            btnClear.setVisible(true);
            anchorHistory.setTranslateX(+195);

            slide.setOnFinished((ActionEvent e) -> {
                historyShow.setVisible(false);
                historyHidden.setVisible(true);

            });
        });
    }


    @FXML
    void searchWord(ActionEvent event) throws IOException {
        String target = textSearch.getText();
        if (translate.containsKey(target)) {
            //textShow.setText(translate.get(target));
            wordBig.setText(target);
            String valueWord = translate.get(target);
            String[] p = valueWord.split("\n", 2);
            spell.setText(p[0]);
            textShow.setText(p[1]);

            textSearch.clear();
            if (hisWord.contains(target)) {
                hisWord.remove(target);
                HandleFile.hisWord.add(0, target);

            } else {
                HandleFile.hisWord.add(0, target);
            }

            historyWord.getItems().clear();
            historyWord.getItems().addAll(hisWord);

            FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/history.txt"));

            for (String s : hisWord) {
                insetFileWriter.append(s).append("\n");
            }
            insetFileWriter.close();

        } else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo");
            alert.setHeaderText("Chưa có từ");
            alert.setContentText("Từ bạn tìm chưa có trong từ điển!");
            alert.showAndWait();
        }

    }

    @FXML
    void speakWord(ActionEvent event) throws EngineException, IOException, JavaLayerException {
        String target = wordBig.getText();
        SpeakFree.speakText(target);
//        Audio u = new Audio();
//        InputStream sound = u.getAudio(target, "en");
//        u.play(sound);

    }

    @FXML
    void switchOnline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOnline.fxml")));
        Scene scene = new Scene(root);
        String css = this.getClass().getResource("online.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchInsert(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("insert.fxml")));
        Scene scene = new Scene(root);
        String css = this.getClass().getResource("insert.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchDelete(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("delete.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchModify(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("modify.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchFavourite(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("favourite.fxml")));
        Scene scene = new Scene(root);
        String css = this.getClass().getResource("insert.css").toExternalForm();
        scene.getStylesheets().add(css);
        stage.setScene(scene);
        stage.show();

    }

    @FXML
    void addToFavourite(ActionEvent event) throws IOException {
        String f = wordBig.getText();
        if (f.isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Thông báo!");
            alert.setContentText("Không có từ nào được thêm vào danh sách yêu thích!");
            alert.showAndWait();
        } else if (HandleFile.favourite.containsKey(f)) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Trùng lặp!");
            alert.setContentText("Từ này đã có trong danh sách yêu thích rồi!");
            alert.showAndWait();
        } else {
            String fMean = textShow.getText();
            String spell1 = spell.getText();
            String value = spell1 + "\n" + fMean;
            HandleFile.favourite.put(f, value);
            HandleFile.fWord.add(f);
            FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/favourite.txt"));
            for (Map.Entry mapElement : HandleFile.favourite.entrySet()) {
                String updateWord = (String) mapElement.getKey();
                String updateMean = (String) mapElement.getValue();
                insetFileWriter.append("@").append(updateWord);
                insetFileWriter.append(" ").append(updateMean).append("\n");

            }
            insetFileWriter.close();

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Thành Công!");
            alert.setContentText("Đã thêm thành công vào danh sách từ yêu thích của bạn!");
            alert.showAndWait();
        }

    }

    @FXML
    void clearHistory(ActionEvent event) throws IOException {
        HandleFile.hisWord.clear();
        historyWord.getItems().clear();
        PrintWriter writer = new PrintWriter("src/main/java/dictxt/history.txt");
        writer.print("");
        writer.close();


    }
}
