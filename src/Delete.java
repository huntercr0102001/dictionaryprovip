package com.example.midterm;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.textfield.TextFields;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import static com.example.midterm.HandleFile.words;

public class Delete implements Initializable {
    @FXML
    private Button btnDelete;

    @FXML
    private Button btnDeleteWord;

    @FXML
    private Button btnInsert;

    @FXML
    private Button btnModify;

    @FXML
    private Button btnOffline;

    @FXML
    private Button btnOnline;

    @FXML
    private Button btnPara;

    @FXML
    private Button btnSpeak1;

    @FXML
    private Label hiddenMenu;

    @FXML
    private ImageView imageDelete;

    @FXML
    private ImageView imageFav;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageOffline;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;

    @FXML
    private ImageView imageOnline;


    @FXML
    private Label showMenu;

    @FXML
    private AnchorPane sld;

    @FXML
    private AnchorPane slider;

    @FXML
    private TextField textSearch;
    @FXML
    private ImageView imageDic;

    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());
    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageDic.setImage(dic);
        imageFav.setImage(favourite);
        imageIcon.setImage(menuIcon);
        imageIconBack.setImage(menuIcon);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }


        TextFields.bindAutoCompletion(textSearch, words);
        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });

    }

    @FXML
    void switchOffline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchOnline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOnline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchInsert(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("insert.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void deleteWord(ActionEvent event) throws IOException {
        String newWord = textSearch.getText();

        if (!HandleFile.translate.containsKey(newWord)) {

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Không có từ!");
            alert.setContentText("Từ bạn muốn xóa không có trong từ điển! Làm ơn kiểm tra lại!");
            alert.showAndWait();
        } else {
            Alert comfirmDelete = new Alert(Alert.AlertType.CONFIRMATION);
            comfirmDelete.setTitle("Cảnh báo");
            comfirmDelete.setHeaderText("Cảnh báo!");
            comfirmDelete.setContentText("Bạn chắc chắn muỗn xóa không?");
            ButtonType buttonTypeYes = new ButtonType("Chắc Chắn", ButtonBar.ButtonData.YES);
            ButtonType buttonTypeNo = new ButtonType("Thoát", ButtonBar.ButtonData.NO);

            comfirmDelete.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
            Optional<ButtonType> result = comfirmDelete.showAndWait();
            if (result.get() == buttonTypeYes) {
                HandleFile.translate.remove(newWord);
                HandleFile.words.remove(newWord);
//                System.out.println(newWord);
                TextFields.bindAutoCompletion(textSearch, words);

                FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/big.txt"));
                for (Map.Entry mapElement : HandleFile.translate.entrySet()) {
                    String updateWord = (String) mapElement.getKey();
                    String updateMean = (String) mapElement.getValue();
                    insetFileWriter.append("@").append(updateWord);
                    insetFileWriter.append(" ").append(updateMean).append("\n");

                }
                insetFileWriter.close();


                int c = HandleFile.words.size();
                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText("Xóa từ thành công!\nTừ điển của bạn có " + c + " từ!");
                a1.showAndWait();
//                textSearch.clear();


            } else {
                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText(message);
//                a1.initOwner(stage.);
                a1.show();
            }
        }
    }

    @FXML
    void switchModify(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("modify.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchFavourite(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("favourite.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
