package com.example.midterm;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.ResourceBundle;

import static com.example.midterm.HandleFile.translate;

public class Insert implements Initializable {

    @FXML
    private Button btnComfirmInsert;

    @FXML
    private Button btnDelete;

    @FXML
    private Button btnInsert;

    @FXML
    private Button btnModify;

    @FXML
    private Button btnOffline;

    @FXML
    private Button btnOnline;

    @FXML
    private Button btnPara;
    @FXML
    private ImageView wifiYes;
    @FXML
    private ImageView wifiNo;

    @FXML
    private Label hiddenMenu;

    @FXML
    private ImageView imageDelete;

    @FXML
    private ImageView imageIcon;

    @FXML
    private ImageView imageIconBack;

    @FXML
    private ImageView imageInsert;

    @FXML
    private ImageView imageModify;

    @FXML
    private ImageView imageOffline;

    @FXML
    private ImageView imageDic;

    @FXML
    private ImageView imageOnline;

    @FXML
    private ImageView imageFav;

    @FXML
    private TextField meanInsert;

    @FXML
    private TextArea textAreaMeaning;

    @FXML
    private Label showMenu;

    @FXML
    private AnchorPane sld;

    @FXML
    private AnchorPane slider;

    @FXML
    private TextField wordInsert;
    Image dic = new Image(new File("src/main/java/image/d3.png").toURI().toString());
    Image offline = new Image(new File("src/main/java/image/off1.png").toURI().toString());
    Image online = new Image(new File("src/main/java/image/on1.png").toURI().toString());
    Image modify = new Image(new File("src/main/java/image/m1.png").toURI().toString());
    Image delete = new Image(new File("src/main/java/image/d1.png").toURI().toString());
    Image insert = new Image(new File("src/main/java/image/add1.png").toURI().toString());
    Image favourite = new Image(new File("src/main/java/image/f7.png").toURI().toString());
    Image menuIcon = new Image(new File("src/main/java/image/icons8-menu-240.png").toURI().toString());
    Image search = new Image(new File("src/main/java/image/search.png").toURI().toString());


    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        imageDelete.setImage(delete);
        imageInsert.setImage(insert);
        imageModify.setImage(modify);
        imageOffline.setImage(offline);
        imageOnline.setImage(online);
        imageFav.setImage(favourite);
        imageIcon.setImage(menuIcon);
        imageDic.setImage(dic);
        imageIconBack.setImage(menuIcon);
        wifiYes.setImage(online);
        wifiNo.setImage(offline);

        try {
            url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            wifiYes.setVisible(true);
            wifiNo.setVisible(false);
        } catch (MalformedURLException e) {
            wifiYes.setVisible(false);
            wifiNo.setVisible(true);
        } catch (IOException e) {
            System.out.println();
        }


        slider.setTranslateX(-165);
        showMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(-165);
            slide.play();

            slider.setTranslateX(0);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(false);
                hiddenMenu.setVisible(true);
            });
        });

        hiddenMenu.setOnMouseClicked(event -> {
            TranslateTransition slide = new TranslateTransition();
            slide.setDuration(Duration.seconds(0.3));
            slide.setNode(slider);

            slide.setToX(0);
            slide.play();

            slider.setTranslateX(-170);

            slide.setOnFinished((ActionEvent e) -> {
                showMenu.setVisible(true);
                hiddenMenu.setVisible(false);
            });
        });

    }

    @FXML
    void switchOffline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOffline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchOnline(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("searchOnline.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchDelete(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("delete.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void enterInsert(ActionEvent event) throws IOException {

        if (wordInsert.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo");
            alert.setHeaderText("Thiếu từ");
            alert.setContentText("Bạn chưa nhập từ mới. Vui lòng kiểm tra lại!");
            alert.showAndWait();
        } else if (meanInsert.getText().length() == 0) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Cảnh báo");
            alert.setHeaderText("Thiếu từ");
            alert.setContentText("Bạn chưa nhập nghĩa của từ. Vui lòng kiểm tra lại!");
            alert.showAndWait();
        } else if (HandleFile.translate.containsKey(wordInsert.getText())) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Cảnh báo");
            alert.setHeaderText("Trùng lặp từ!");
            alert.setContentText("Từ bạn nhập đã có trong từ điển, Bạn có muốn ghi đè không?");
            ButtonType buttonTypeYes = new ButtonType("Yes", ButtonBar.ButtonData.YES);
            ButtonType buttonTypeNo = new ButtonType("No", ButtonBar.ButtonData.NO);

            alert.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeYes) {


                String newWord = wordInsert.getText();
                String spell = meanInsert.getText();
                String meanWord = textAreaMeaning.getText();
                String value = spell + "\n" + meanWord;

                HandleFile.translate.replace(newWord, value);

                FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/big.txt"));
                for (Map.Entry mapElement : HandleFile.translate.entrySet()) {
                    String updateWord = (String) mapElement.getKey();
                    String updateMean = (String) mapElement.getValue();
                    insetFileWriter.append("@").append(updateWord);
                    insetFileWriter.append(" ").append(updateMean).append("\n");
                }
                insetFileWriter.close();

                wordInsert.clear();
                meanInsert.clear();
                textAreaMeaning.clear();
                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText("Từ của bạn đã được cập nhật!");
                a1.show();


            } else {
                String message = result.get().getText();
                Alert a1 = new Alert(Alert.AlertType.INFORMATION);
                a1.setContentText("Làm ơn chèn lại!");
                a1.show();
            }


        } else {


                String englishWord = wordInsert.getText();
                String spell = meanInsert.getText();
                String vnWord = textAreaMeaning.getText();
                String value = spell + "\n" + vnWord;

                translate.put(englishWord, value);
                HandleFile.words.add(englishWord);

                // ghi lai vao file
            FileWriter insetFileWriter = new FileWriter(new File("src/main/java/dictxt/big.txt"));
            for (Map.Entry mapElement : HandleFile.translate.entrySet()) {
                String updateWord = (String) mapElement.getKey();
                String updateMean = (String) mapElement.getValue();
                insetFileWriter.append("@").append(updateWord);
                insetFileWriter.append(" ").append(updateMean).append("\n");

            }
            insetFileWriter.close();
            wordInsert.clear();
            meanInsert.clear();
            textAreaMeaning.clear();

            int c = HandleFile.words.size();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Thông báo");
            alert.setHeaderText("Thành Công!");
            alert.setContentText("Thêm từ thành công!\nTừ điển của bạn có " + c + " từ!");
            alert.showAndWait();
        }

    }

    @FXML
    void switchModify(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("modify.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    void switchFavourite(ActionEvent event) throws IOException {
        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        Parent root = FXMLLoader.load(Objects.requireNonNull(this.getClass().getResource("favourite.fxml")));
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}
